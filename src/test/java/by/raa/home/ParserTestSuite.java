package by.raa.home;

import by.raa.home.parser.MathExpressionParserTest;

import by.raa.home.util.ExpressionFromFileTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
MathExpressionParserTest.class,
ExpressionFromFileTest.class
})
public class ParserTestSuite {
}
