package by.raa.home.util;

import java.io.FileReader;
import java.io.FileWriter;

import static org.junit.Assert.assertArrayEquals;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

public class ExpressionFromFileTest {

    FileReader inputData = null;
    FileWriter outputData = null;
    ExpressionFromFile file = new ExpressionFromFile();
    String[] expression = { "2+2" };

    @Test
    public void readAndWriteDataFromFileTest() {
        try {
            outputData = new FileWriter("src/test/resources/mathTest.txt");
            assertNotNull(outputData);
            file.writeDataToFile(outputData, expression);
            outputData.close();
            inputData = new FileReader("src/test/resources/mathTest.txt");
            assertNotNull(inputData);
            assertArrayEquals(expression, file.readDataFromFile(inputData));
        } catch (IOException e) {
            System.out.println("Don't write data to file");
        } finally {
            try {
                outputData.close();
            } catch (IOException e) {
                System.out.println("Error close file after write");
            }
        }

        try {
            inputData = new FileReader("src/test/resources/mathTest.txt");
            assertNotNull(inputData);
            assertArrayEquals(expression, file.readDataFromFile(inputData));
        } catch (IOException e) {
            System.out.println("Don't read data from file");
        }
        finally {
                    try {
                        outputData.close();
                    } catch (IOException e) {
                        System.out.println("Error close file after read");
                    }
                }


    }


}
