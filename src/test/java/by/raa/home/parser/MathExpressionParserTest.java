package by.raa.home.parser;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MathExpressionParserTest {

    Double msg =
        Math.sin(Math.toRadians(30)) + Math.cos(Math.toRadians(60)) - Math.tan(Math.toRadians(45)) +
        (2 * 2) * Math.log(3) - (4 / 2) * Math.sqrt(9)+Math.pow(2, 3);
    MathExpressionParser parser = new MathExpressionParser();

    @Test
    public void testParse() {

        try {
            parser.parse("sin(30)+cos(60)-tg(45)+(2*2)*log(3)-(4/2)*sqrt(9)+pow(2,3)");
            assertEquals(msg, parser.evaluate());

        } catch (ParseException e) {
            System.out.println("Parse error");
        }
    }
}
