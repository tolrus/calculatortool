package by.raa.home;

import by.raa.home.parser.MathExpressionParserTest;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class RunnerTest {
     
        public static void main(String[] args) {
        Result result = JUnitCore.runClasses(ParserTestSuite.class);
        for (Failure failure : result.getFailures()) {
        System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
        }
    }

