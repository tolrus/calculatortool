package by.raa.home.parser;

import java.text.ParseException;

import java.util.Collections;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class MathExpressionParser {


    private final String[] FUNCTIONS = { "sqrt", "pow", "sin", "cos", "tg", "log" };

    private final String OPERATORS = "+-*/";

    private final String SEPARATOR = ",";

    private LinkedList<String> operations = new LinkedList<String>();

    public LinkedList<String> reversePN = new LinkedList<String>();

    private LinkedList<Double> answer = new LinkedList<Double>();

    private boolean isNumber(String restOfLine) {
        try {
            Double.parseDouble(restOfLine);
        } catch (Exception e) {

            return false;
        }
        return true;
    }

    private boolean isFunction(String restOfLine) {
        for (String item : FUNCTIONS) {
            if (item.equals(restOfLine)) {
                return true;
            }
        }
        return false;
    }

    private boolean isSeparator(String restOfLine) {
        return restOfLine.equals(SEPARATOR);
    }

    private boolean isOpenBracket(String restOfLine) {
        return restOfLine.equals("(");
    }

    private boolean isCloseBracket(String restOfLine) {
        return restOfLine.equals(")");
    }

    private boolean isOperator(String restOfLine) {
        return OPERATORS.contains(restOfLine);
    }

    private byte getPrioretit(String restOfLine) {
        if (restOfLine.equals("+") || restOfLine.equals("-")) {
            return 1;
        }
        return 2;
    }

    public void parse(String expression) throws ParseException {

        this.operations.clear();
        this.reversePN.clear();


        expression = expression.replace(" ", "").replace("(-", "(0-").replace(",-", ",0-");
        if (expression.charAt(0) == '-' || expression.charAt(0) == '+') {
            expression = "0" + expression;
        }

        StringTokenizer stringTokenizer = new StringTokenizer(expression, OPERATORS + SEPARATOR + "()", true);


        while (stringTokenizer.hasMoreTokens()) {


            String restOfLine = stringTokenizer.nextToken();

            if (isSeparator(restOfLine)) {
                while (!this.operations.isEmpty() && !this.isOpenBracket(this.operations.getFirst())) {
                    this.reversePN.push(this.operations.pop());
                }
            } else if (this.isNumber(restOfLine)) {
                this.reversePN.push(restOfLine);
            } else if (this.isFunction(restOfLine)) {
                this.operations.push(restOfLine);

            } else if (this.isOpenBracket(restOfLine)) {
                this.operations.push(restOfLine);

            } else if (isCloseBracket(restOfLine)) {

                while (!this.operations.isEmpty() && !this.isOpenBracket(this.operations.getFirst())) {
                    this.reversePN.push(this.operations.pop());
                }

                this.operations.pop();
                if (!this.operations.isEmpty() && this.isFunction(this.operations.getFirst())) {
                    this.reversePN.push(this.operations.pop());
                }
            } else if (isOperator(restOfLine)) {
                while (!this.operations.isEmpty() && this.isOperator(this.operations.getFirst()) &&
                       this.getPrioretit(restOfLine) <= this.getPrioretit(this.operations.getFirst())) {
                    this.reversePN.push(this.operations.pop());
                }
                this.operations.push(restOfLine);
            } else {
                throw new ParseException("Can't parse rest of line", 0);
            }

        }
        while (!this.operations.isEmpty()) {
            this.reversePN.push(this.operations.pop());
        }

        Collections.reverse(reversePN);
    }


    public Double evaluate() throws ParseException {

        if (this.reversePN.isEmpty()) {
            return 0.0;
        }
        this.answer.clear();

        while (!this.reversePN.isEmpty()) {
            String restOfLine = this.reversePN.pop();
            if (isNumber(restOfLine)) {
                this.answer.push(Double.parseDouble(restOfLine));
            } else if (isOperator(restOfLine)) {
                Double a = answer.pop();
                Double b = answer.pop();
                if (restOfLine.equals("+")) {
                    this.answer.push(b + a);
                } else if (restOfLine.equals("-")) {
                    this.answer.push(b - a);
                } else if (restOfLine.equals("*")) {
                    this.answer.push(b * a);
                } else if (restOfLine.equals("/")) {
                    this.answer.push(b / a);
                }
            } else if (isFunction(restOfLine)) {
                Double a = answer.pop();
                if (restOfLine.equals("sqrt")) {
                    this.answer.push(Math.sqrt(a));
                } else if (restOfLine.equals("pow")) {
                    Double b = answer.pop();
                    this.answer.push(Math.pow(b, a));
                } else if (restOfLine.equals("sin")) {
                    this.answer.push(Math.sin(Math.toRadians(a)));
                } else if (restOfLine.equals("cos")) {
                    this.answer.push(Math.cos(Math.toRadians(a)));
                } else if (restOfLine.equals("tg")) {
                    this.answer.push(Math.tan(Math.toRadians(a)));

                } else if (restOfLine.equals("log")) {
                    this.answer.push(Math.log(a));
                }
            }
        }

        if (this.answer.size() > 1) {
            throw new ParseException("Operator is missing", 0);
        }

        return this.answer.pop();
    }


}


