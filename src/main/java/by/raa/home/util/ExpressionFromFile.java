package by.raa.home.util;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.StringTokenizer;

public class ExpressionFromFile {

    private String allData;
    private String expressions[];

    private String readData(FileReader inputData) throws IOException {

        this.allData = "";
        int count;
        while ((count = inputData.read()) != -1) {
            allData += (char) count;
        }
        return allData;
    }

    private void writeData(FileWriter outputData, String expression) throws IOException {

        outputData.write(expression);

    }

    public String[] readDataFromFile(FileReader inputData) throws IOException {

        StringTokenizer token = new StringTokenizer(this.readData(inputData), System.getProperty("line.separator"));
        this.expressions = new String[token.countTokens()];
        int count = 0;
        while (token.hasMoreTokens()) {
            this.expressions[count] = token.nextToken();
            count++;
        }
        return this.expressions;

    }

    public void writeDataToFile(FileWriter outputData, String expression[]) throws IOException {
        for (int i = 0; i < expression.length; i++) {

            this.writeData(outputData, expression[i]);
        }
    }

}
