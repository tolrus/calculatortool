package by.raa.home;

import by.raa.home.parser.MathExpressionParser;
import by.raa.home.util.ExpressionFromFile;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.ParseException;

import java.util.StringTokenizer;

public class CalculatorTool {
    public static void main(String[] args) {

        String[] expressions = null;
        String[] interimResult = null;
        String[] result = null;
        MathExpressionParser parser = new MathExpressionParser();
        ExpressionFromFile file = new ExpressionFromFile();
        FileReader inputData = null;
        FileWriter outputData = null;

        try {
            inputData = new FileReader("src/main/resources/math.txt");
            outputData = new FileWriter("src/main/resources/math_copy.txt", true);          
            expressions =file.readDataFromFile(inputData);            
            interimResult = new String[expressions.length];
            result = new String[expressions.length];          

            for (int i = 0; i < expressions.length; i++) {
                parser.parse(expressions[i]);
                interimResult[i] = Double.toString(parser.evaluate());
                result[i] = expressions[i] + "=" + interimResult[i] + System.getProperty("line.separator");
            }
            
            file.writeDataToFile(outputData, result);


        } catch (ParseException e) {
            System.out.println("Parse error");
        } catch (FileNotFoundException e) {
            System.out.println("Open file error.");
        } catch (IOException e) {
            System.out.println("Read file error.");
        } finally {
            try {
                inputData.close();
                outputData.close();
            } catch (IOException e) {
                System.out.println("Close input file error.");
            }

        }

    }
}


